# About

This project was created as part of my training in Angular development
while also promoting eco-environmental care.  
App is live at **https://ecofriendly-bf847.web.app/home**
Admin panel located in top right.

## Startup

1. Install dependencies using **npm install**
2. Start the development **ng serve**
3. Open your web browser and navigate to **http://localhost:4200**

## Hosting & Deployment

Hosted and deployed using [Firebase](https://firebase.google.com/)

## Libraries used

- **ngx-disquss** integrate Facebook features such as likes and share
- **ngx-fb** is utilized to integrate Disqus for social interaction
- **ngx-pagination** is utilized to integrate Disqus for social interaction
- **@angular/fire**,**firebase** bindings for Firebase services
- **@tinymce/tinymce-angular** integrates the TinyMCE WYSIWYG editor enabling rich text editing capabilities
- **@moment** for parsing manipulating dates and times

# Note

Please note that some external libraries, such as **ngx-disqus** may not function as expected
due to factors such as changes in the library's API or expiration of API keys

Why did my CSS end up looking like that? 
https://www.youtube.com/watch?v=RsYi2VH2y6U

But really, this was early project and experience was gained :)
