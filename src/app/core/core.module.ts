import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BackgroundComponent } from './background/background.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [HeaderComponent, BackgroundComponent, FooterComponent],
  imports: [CommonModule, RouterModule],
  exports: [HeaderComponent, BackgroundComponent, FooterComponent],
})
export class CoreModule {}
