import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { IMAGE_URLS } from '../../shared/models/image-urls';

@Component({
  selector: 'app-background',
  templateUrl: './background.component.html',
  styleUrls: ['./background.component.scss'],
})
export class BackgroundComponent implements OnInit, OnDestroy {
  everyImgUrl = IMAGE_URLS;
  currentImageIndex = 0;
  imageUrl: string = '';
  timedBackground: any;

  constructor(private cdr: ChangeDetectorRef) {}

  ngOnInit(): void {
    // this.timedBackground = setInterval(() => {
    //   this.currentImageIndex =
    //     (this.currentImageIndex + 1) % this.everyImgUrl.length;
    //   this.imageUrl = 'url(' + this.everyImgUrl[this.currentImageIndex] + ')';
    //   this.cdr.detectChanges();
    // }, 60000);
  }

  ngOnDestroy(): void {
    clearInterval(this.timedBackground);
  }
}
