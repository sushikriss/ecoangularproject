import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  constructor(private afAuth: AngularFireAuth, private auth: AuthService) {}
  showMenu: boolean = false;

  returnAfAuth(): AngularFireAuth {
    return this.afAuth;
  }

  logout(): void {
    this.auth.logout();
  }
}
