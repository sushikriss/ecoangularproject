import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'momentFormat',
})
export class MomentFormatPipe implements PipeTransform {
  transform(value: any, format: string): any {
    moment.locale('en');
    return moment(value).format(format);
  }
}
