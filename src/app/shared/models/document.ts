export class docObject {
  'name': string;
  'fileUrl': string;
}

export class articleObject {
  'dateCreated': string;
  'contentBody': string;
  'imageURL': string;
  'uniqueId': string;
}

export interface articleView {
  'title': string;
  'content': string;
  'dateCreated': string;
}

export const yearsArray = [
  '2026',
  '2025',
  '2024',
  '2023',
  '2022',
  '2021',
  '2020',
  '2019',
  '2018',
  '2017',
  '2016',
  '2015',
  '2014',
];
