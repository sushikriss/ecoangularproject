// import { Injectable } from '@angular/core';
// import { AngularFireAuth } from '@angular/fire/compat/auth';
// import { CanActivate, Router } from '@angular/router';

// @Injectable({
//   providedIn: 'root',
// })
// export class AuthGuardService implements CanActivate {
//   constructor(private router: Router, private afAuth: AngularFireAuth) {}

//   canActivate(): Promise<boolean>{
//     return new Promise((resolve) => {
//       this.afAuth.onAuthStateChanged((user) => {
//         if (user) {
//           resolve(true);
//         } else {
//           this.router.navigate(['/admin']);
//           resolve(false);
//         }
//       });
//     });
//   }
// }
