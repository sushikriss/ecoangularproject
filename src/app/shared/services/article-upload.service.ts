import { Injectable } from '@angular/core';
import {
  deleteObject,
  getDownloadURL,
  getMetadata,
  getStorage,
  listAll,
  ref,
  uploadString,
} from 'firebase/storage';
import * as uuid from 'uuid';
import { articleObject } from '../models/document';
@Injectable({
  providedIn: 'root',
})
export class ArticleUploadService {
  constructor() {}

  // IMAGE transformed to HTML Element
  formatImage($event: Event): File | null {
    const event = $event.target as HTMLInputElement;
    const selectedFile = null;
    if (event.files) {
      const selectedFile = event.files[0];
      return selectedFile;
    }
    return selectedFile;
  }

  // Fields validation
  formIsValid(title: string, content: string): boolean {
    if (title === '' || content === '' || content === undefined) {
      return false;
    }
    return true;
  }

  // Uploads article to Firebase Storage
  async uploadArticle(
    title: string,
    articleContent: string,
    image: File | null
  ): Promise<void> {
    let imageURL = 'null';
    if (image !== null) {
      imageURL = await this.convertImageToDataUrl(image);
    }

    const storage = getStorage();
    const uniqueId = uuid.v4();
    const timeCreated = new Date().toISOString();
    const storageRef = ref(storage, `/public/news/${title}`);
    const metadata = {
      contentType: 'text/html',
      customMetadata: {
        dateCreated: timeCreated,
        imageURL: imageURL,
        uniqueId: uniqueId,
      },
    };

    await uploadString(storageRef, articleContent, 'raw', metadata).then(
      (response) => {}
    );
  }

  // Converts the image to dataURL to be saved in the storage
  async convertImageToDataUrl(file: File): Promise<string> {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.onloadend = (e) => {
        const dataUrl = fileReader.result as string;
        resolve(dataUrl);
      };
      fileReader.onerror = (e) => reject(e);
      fileReader.readAsDataURL(file);
    });
  }

  // Returns the articles from the storage
  async returnArticles(): Promise<[string, articleObject][]> {
    const storage = getStorage();
    const listRef = ref(storage, 'public/news');
    const articlesTitleContentMap = new Map<string, articleObject>();
    

    try {
      const prefixesItems = await listAll(listRef);
      await Promise.all(
        prefixesItems.items.map(async (article) => {
          const articleTitleKey = article.name;
          const dateCreated = (await getMetadata(article)).customMetadata![
            'dateCreated'
          ];
          const imageURL = (await getMetadata(article)).customMetadata![
            'imageURL'
          ];
          const uniqueId = (await getMetadata(article)).customMetadata![
            'uniqueId'
          ];
          const articleURL = await getDownloadURL(article);
          const contentBody = await (await fetch(articleURL)).text();
          const dateAndContentValue = {
            dateCreated: dateCreated,
            contentBody: contentBody,
            imageURL: imageURL,
            articleURL: articleURL,
            uniqueId: uniqueId,
          };
          articlesTitleContentMap.set(articleTitleKey, dateAndContentValue);
        })
      );
    } catch (error) {
      console.log(error);
    }

    return this.sortArticlesByDate(articlesTitleContentMap);
  }

  // Sorts articles by date: latest is the first, earliest is the last
  sortArticlesByDate(
    map: Map<string, articleObject>
  ): [string, articleObject][] {
    const sortedArticles = [...map.entries()].sort((a, b) => {
      return Date.parse(b[1].dateCreated) - Date.parse(a[1].dateCreated);
    });
    return sortedArticles;
  }

  deleteFile(articleURL: string): boolean {
    const storage = getStorage();
    const listRef = ref(storage, `public/news/${articleURL}`);
    deleteObject(listRef)
      .then(() => {
        return true;
      })
      .catch((error) => {
        console.log(error);
      });
    return false;
  }
}
