import { TestBed } from '@angular/core/testing';

import { ArticleUploadService } from './article-upload.service';

describe('ArticleUploadService', () => {
  let service: ArticleUploadService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ArticleUploadService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
