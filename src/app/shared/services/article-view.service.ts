import { Injectable } from '@angular/core';
import {
  getDownloadURL,
  getMetadata,
  getStorage,
  listAll,
  ref,
} from 'firebase/storage';
import { articleView } from '../models/document';

@Injectable({
  providedIn: 'root',
})
export class ArticleViewService {
  constructor() {}

  // Find the article by ID
  async findArticleById(articleId: string): Promise<articleView> {
    const storage = getStorage();
    const listRef = ref(storage, 'public/news');
    const articleContent: articleView = {
      title: '',
      content: '',
      dateCreated: '',
    };

    try {
      const prefixesItems = await listAll(listRef);
      for (const item of prefixesItems.items) {
        const articleDataId = (await getMetadata(item)).customMetadata![
          'uniqueId'
        ];
        if (articleDataId === articleId) {
          const date = (await getMetadata(item)).customMetadata!['dateCreated'];
          const articleURL = await getDownloadURL(item);
          const contentBody = await (await fetch(articleURL)).text();
          articleContent.title = item.name;
          articleContent.content = contentBody;
          articleContent.dateCreated = date;
          break;
        }
      }
    } catch (error) {
      console.log(error);
    }
    return articleContent;
  }
}
