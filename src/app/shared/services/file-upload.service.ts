import { EventEmitter, Injectable } from '@angular/core';
import {
  deleteObject,
  getDownloadURL,
  getStorage,
  listAll,
  ref,
  uploadBytesResumable
} from 'firebase/storage';
import { docObject } from '../models/document';

@Injectable({
  providedIn: 'root',
})
export class FileUploadService {
  private storage = getStorage();
  constructor() {}

  // Using EventEmitter to send the progress back to the public.componet
  public progressEmitter = new EventEmitter<string>();

  // Handles the document and returns it back to the public.componet
  uploadPdfService($event: Event): File | null {
    const event = $event.target as HTMLInputElement;
    const selectedFile = null;
    if (event.files) {
      const selectedFile = event.files[0];
      return selectedFile;
    }
    return selectedFile;
  }

  // Saves the document to Firebase and emits the progress bar
  saveDocumentService(
    title: string,
    year: string,
    file: File,
    date: Date
  ): boolean {
    const metadata = {
      contentType: 'application/pdf',
      customMetadata: {
        title: title,
        year: year,
        date: new Date(date).toISOString(),
      },
    };

    const docsRef = ref(this.storage, `public/documents/${year}/${title}`);
    console.log(metadata);

    const task = uploadBytesResumable(docsRef, file, metadata);
    task.on(
      'state_changed',
      (snapshot) => {
        const progress =
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        this.progressEmitter.emit(progress.toFixed(0));
      },
      (error) => {
        error.message = 'Firebase upload failed: Limit reached?';
        throw new Error(error.message);
      }
    );
    return true;
  }

  // Fields validation
  formIsValid(title: string, file: File | null): boolean {
    if (title === '' || file === null) {
      return false;
    }
    return true;
  }

  // Sorts the map and returns it as array
  sortByYearDescArr(mapUsed: Map<number, docObject[]>):  [number, docObject[]][] {
    const sortedArray = Array.from(mapUsed.entries())
      .sort((a, b) => b[0] - a[0])
      .map((entry) => {
        entry[1].sort((a, b) => a.name.localeCompare(b.name));
        return entry;
      });
    return sortedArray;
  }

  // Returns all documents sorted by earliest year
  async getDocuments() {
    const listRef = ref(this.storage, 'public/documents');
    const collectedDataMap = new Map<number, docObject[]>();

    try {
      const resultList = await listAll(listRef);
      await Promise.all(
        resultList.prefixes.map(async (yearRef) => {
          const yearFolder = await listAll(yearRef);
          const yearName = parseInt(yearRef.name);
          const allDocumentsForTheCurrentYear: docObject[] = [];
          await Promise.all(
            yearFolder.items.map(async (item) => {
              const docFileName = item.name;
              const url = await getDownloadURL(item);
              const dataObj = {
                name: docFileName,
                fileUrl: url,
              };
              allDocumentsForTheCurrentYear.push(dataObj);
            })
          );
          collectedDataMap.set(yearName, allDocumentsForTheCurrentYear);
        })
      );
    } catch (error) {
      console.log(error);
    }

    return this.sortByYearDescArr(collectedDataMap);
  }

  // Removes document
  deleteFile(path: any): boolean {
    const deleteRef = ref(this.storage, path);
    deleteObject(deleteRef)
      .then(() => {
        return true;
      })
      .catch((error) => {
        console.log(error);
      });
    return false;
  }
}
