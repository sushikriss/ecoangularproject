import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private afAuth: AngularFireAuth, private router: Router) {}

  // User login
  async loginWithUserPassword(user: string, password: string): Promise<any> {
    try {
      await this.afAuth.signInWithEmailAndPassword(user, password);
      return true;
    } catch (err) {}
  }

  // User logout
  logout(): void {
    this.afAuth
      .signOut()
      .then(() => {
        this.router.navigate(['/admin']);
      })
      .catch((err) => {
        console.log(err);
      });
  }
}
