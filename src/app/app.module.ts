import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { FeatureModule } from './feature/feature.module';
import { SharedModule } from './shared/shared.module';

//Rich text editor
import { EditorModule } from '@tinymce/tinymce-angular';

// Services
import { ArticleUploadService } from './shared/services/article-upload.service';
import { AuthService } from './shared/services/auth.service';
import { FileUploadService } from './shared/services/file-upload.service';

// Admin
import { AdminModule } from './admin/admin.module';

// Firebase. etc
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { AngularFireDatabaseModule } from '@angular/fire/compat/database';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { AngularFireStorageModule } from '@angular/fire/compat/storage';
import { DisqusModule } from 'ngx-disqus';
import { FacebookModule } from 'ngx-facebook';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [AppComponent],
  imports: [
    NgxPaginationModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    CoreModule,
    FeatureModule,
    RouterModule,
    AdminModule,
    SharedModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    FormsModule,
    EditorModule,
    FacebookModule.forRoot(),
    DisqusModule.forRoot('http-localhost-4200-ogorhtr4je'),
  ],
  providers: [AuthService, FileUploadService, ArticleUploadService],
  bootstrap: [AppComponent],
})
export class AppModule {}
