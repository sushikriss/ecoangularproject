import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './admin/login/login.component';
import { AboutComponent } from './feature/about/about.component';
import { ArticleViewComponent } from './feature/article-view/article-view.component';
import { HomeComponent } from './feature/home/home.component';
import { NewsComponent } from './feature/news/news.component';
import { PublicComponent } from './feature/public/public.component';
import { StructureComponent } from './feature/structure/structure.component';

const routes: Routes = [
  // Normal routes
  { path: 'home', component: HomeComponent },
  // { path: 'about', component: AboutComponent },
  { path: 'news', component: NewsComponent },
  { path: 'public', component: PublicComponent },
  { path: 'structure', component: StructureComponent },
  { path: 'news/article/:id', component: ArticleViewComponent },

  // Admin routes
  { path: 'admin', component: LoginComponent },

  // Rest
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', redirectTo: '/home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
