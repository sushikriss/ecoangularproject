import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  constructor(
    private authService: AuthService,
    private router: Router,
    private titleService: Title
  ) {
    this.titleService.setTitle('Admin Login');
  }
  errorMessage?: string;

  loginWithCredentials(form: NgForm): void {
    if (form.valid) {
      const email = form.value.email.toLowerCase();
      const password = form.value.password;

      this.authService
        .loginWithUserPassword(email, password)
        .then((response) => {
          if (response === true) {
            this.router.navigate(['home/admin']);
          } else {
            this.errorMessage = 'Wrong user or password';
          }
        });
    }
  }

  navigateToHome(): void {
    this.router.navigate(['home']);
  }
}
