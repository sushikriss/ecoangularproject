import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FacebookService, UIParams, UIResponse } from 'ngx-facebook';
import { articleView } from 'src/app/shared/models/document';
import { ArticleViewService } from 'src/app/shared/services/article-view.service';

@Component({
  selector: 'app-article-view',
  templateUrl: './article-view.component.html',
  styleUrls: ['./article-view.component.scss'],
})
export class ArticleViewComponent implements OnInit, OnDestroy {
  id: string | null = null;
  article: articleView | null = null;
  title: string = '';
  articleLoaded: boolean = false;
  articleURL: string = '';
  twitterShare: string = '';
  timeout: any = null;
  isMobile: boolean = false;
  constructor(
    private route: ActivatedRoute,
    private articleService: ArticleViewService,
    private fb: FacebookService
  ) {}

  async ngOnInit(): Promise<void> {
    this.id = this.route.snapshot.paramMap.get('id');
    this.article = await this.articleService.findArticleById(this.id!);
    this.articleLoaded = true;
    this.articleURL = window.location.href;
    this.twitterShare = 'EcoFriendly - News%0A' + this.articleURL;
    this.isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    if (this.isMobile) {
      this.article.content = this.article.content.replace(
        /<img(.*?)>/g,
        function (random, attributes) {
          return '<img ' + attributes + ' style="width: 200px; height: auto;">';
        }
      );
    } else {
      this.article.content = this.article.content.replace(
        /<img(.*?)>/g,
        function (random, attributes) {
          return '<img ' + attributes + ' style="width: 600px; height: auto;">';
        }
      );
    }
  }

  ngOnDestroy(): void {
    clearTimeout(this.timeout);
  }

  sharePostFacebook(url: string): void {
    const params: UIParams = {
      href: url,
      method: 'share',
    };

    this.fb
      .ui(params)
      .then((res: UIResponse) => console.log(res))
      .catch((e: any) => console.error(e));
  }

  printArticle(): void {
    window.print();
  }
}
