import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { DomSanitizer, SafeHtml, Title } from '@angular/platform-browser';
import { FacebookService, UIParams, UIResponse } from 'ngx-facebook';
import { Subscription } from 'rxjs';
import { ArticleUploadService } from 'src/app/shared/services/article-upload.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
})
export class NewsComponent implements OnInit {
  // API variables
  location: any;
  weather: any;
  weatherSubscription: Subscription | undefined;
  twitterShare: any;
  pageURL: any;
  // Inputs variables
  showArticleForm: boolean = false;
  title: string = '';
  editorContent: any;

  // File variables
  selectedFileName: string = 'Default picture set - ECOFRIENDLY.png';
  selectedArticleImage: File | null = null;
  returnedArticles: any;
  defaultArticleImage: string = 'assets/img/ecofriendly.jpg';
  selectedElement: any;
  applyStylesPublic: boolean = false;
  page: number = 1;
  count: number = 5;

  // Alert message variables
  showErrorMessage: boolean = false;
  articlesLoaded: boolean = false;
  showPopupConfirmation: boolean = false;
  fileToDelete: string = '';
  fileDeleteUrl: string = '';
  timeoutUsed: any;

  constructor(
    private fb: FacebookService,
    private http: HttpClient,
    private afAuth: AngularFireAuth,
    private articleService: ArticleUploadService,
    private domSanitizer: DomSanitizer,
    private titleService: Title
  ) {
    this.titleService.setTitle('EcoFriendly | News');
    this.pageURL = window.location.href;
    this.twitterShare = 'EcoFriendly - News%0A' + this.pageURL;
  }

  // Sanitizing content
  transform(html: string): SafeHtml {
    return this.domSanitizer.bypassSecurityTrustHtml(html);
  }

  returnAfAuth(): AngularFireAuth {
    return this.afAuth;
  }

  async ngOnInit(): Promise<void> {
    const response = await fetch('https://ipapi.co/json/');
    this.location = await response.json();

    this.weatherSubscription = this.http
      .get(
        `https://api.weatherapi.com/v1/current.json?key=42ed8c8bdc364730926101855231901&q=${this.location.city}&aqi=no`
      )
      .subscribe((data) => {
        this.weather = data;
      });
      
    this.returnedArticles = await this.articleService.returnArticles();
    this.articlesLoaded = true;
  }

  ngOnDestroy(): void {
    if (this.weatherSubscription) {
      this.weatherSubscription.unsubscribe();
    }
    clearTimeout(this.timeoutUsed);
  }

  onSubmitArticle(title: string, editorContent: string): void {
    if (this.articleService.formIsValid(title, editorContent)) {
      const content = editorContent;
      const finishedUploading = this.articleService.uploadArticle(
        title,
        content,
        this.selectedArticleImage
      );
      finishedUploading
        .then(() => {
          window.location.reload();
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      this.showErrorMessage = true;
      this.timeoutUsed = setTimeout(() => {
        this.showErrorMessage = false;
      }, 4500);
    }
  }

  // IMAGE upload button function
  uploadImage(event: Event): void {
    // Formats the image to HTML Element
    const returnedImageAsElement = this.articleService.formatImage(event);
    // Checks if the image exists and appends name and file
    if (returnedImageAsElement) {
      this.selectedFileName = returnedImageAsElement.name;
      this.selectedArticleImage = returnedImageAsElement;
    }
  }

  // Facebook API share
  sharePostFacebook(url: string): void {
    const params: UIParams = {
      href: url,
      method: 'share',
    };

    this.fb
      .ui(params)
      .then((res: UIResponse) => console.log(res))
      .catch((e: any) => console.error(e));
  }

  toggleExpand(index: any): void {
    this.selectedElement = index;
  }

  // Article deletion
  deleteArticle(path: string): void {
    this.articleService.deleteFile(path);
    this.showPopupConfirmation = false;
    window.location.reload();
  }
}
