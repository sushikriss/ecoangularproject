import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { EditorModule } from '@tinymce/tinymce-angular';
import { DisqusModule } from 'ngx-disqus';
import { FacebookModule } from 'ngx-facebook';
import { NgxPaginationModule } from 'ngx-pagination';
import { MomentFormatPipe } from '../shared/models/date-locale';
import { HtmlSanitizerPipe } from '../shared/models/sanitize-pipe';
import { AboutComponent } from './about/about.component';
import { ArticleViewComponent } from './article-view/article-view.component';
import { HomeComponent } from './home/home.component';
import { NewsComponent } from './news/news.component';
import { PublicComponent } from './public/public.component';
import { StructureComponent } from './structure/structure.component';

@NgModule({
  declarations: [
    HomeComponent,
    AboutComponent,
    StructureComponent,
    NewsComponent,
    PublicComponent,
    HtmlSanitizerPipe,
    MomentFormatPipe,
    ArticleViewComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    EditorModule,
    NgxPaginationModule,
    FacebookModule,
    DisqusModule,
  ],
  exports: [
    HomeComponent,
    AboutComponent,
    StructureComponent,
    NewsComponent,
    PublicComponent,
    ArticleViewComponent,
  ],
})
export class FeatureModule {}
