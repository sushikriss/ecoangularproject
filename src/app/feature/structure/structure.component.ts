import { Component, HostListener } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-structure',
  templateUrl: './structure.component.html',
  styleUrls: ['./structure.component.scss'],
})
export class StructureComponent {
  showButton = false;

  constructor(private titleService: Title) {
    this.titleService.setTitle('EcoFriendly | Impact');
  }

  @HostListener('window:scroll', [])
  onWindowScroll(): void {
    this.showButton = window.pageYOffset > 800;
  }

  returnToTop(): void {
    window.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
  }
}
