import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  constructor(private afAuth: AngularFireAuth, private titleService: Title) {
    this.titleService.setTitle('EcoFriendly | Home');
  }

  returnAfAuth(): AngularFireAuth {
    return this.afAuth;
  }
}
