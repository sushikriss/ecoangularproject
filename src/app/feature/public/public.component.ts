import { Component, OnDestroy, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Title } from '@angular/platform-browser';
import { FileUploadService } from 'src/app/shared/services/file-upload.service';
import { docObject, yearsArray } from '../../shared/models/document';

@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
  styleUrls: ['./public.component.scss'],
})
export class PublicComponent implements OnInit, OnDestroy {
  year: string = '2023';
  yearValues: string[] = yearsArray;
  returnedDocuments: [number, docObject[]][] | undefined;
  title: string = '';
  selectedFile: File | null = null;
  selectedPdfFileName: string = 'Select document';
  showErrorMessage: boolean = false;
  progress: string = '0';
  eventSubscribtion: any;
  showForm: boolean = false;
  showPopupConfirmation = false;
  fileToDelete: string = '';
  fileDeleteUrl: string = '';
  docsLoaded: boolean = false;
  timeoutUsed: any;

  constructor(
    private uploadService: FileUploadService,
    private afAuth: AngularFireAuth,
    private titleService: Title
  ) {
    this.titleService.setTitle('EcoFriendly | Publications');
  }

  returnAfAuth(): AngularFireAuth {
    return this.afAuth;
  }

  // Loads all the documents for the component
  async ngOnInit(): Promise<void> {
    this.returnedDocuments = await this.uploadService.getDocuments();
    this.docsLoaded = true;
  }

  ngOnDestroy(): void {
    this.eventSubscribtion?.unsubscribe();
    clearTimeout(this.timeoutUsed);
  }

  // PDF/document upload
  uploadPdf($event: Event): void {
    this.selectedFile = this.uploadService.uploadPdfService($event);
    if (this.selectedFile) {
      this.selectedPdfFileName = this.selectedFile.name;
      this.title = this.selectedFile.name.replace(/\.pdf$/i, '');
    }
  }

  // PDF saved to Firebase
  saveDocument(): void {
    // Subscribing to emit the progress
    this.eventSubscribtion = this.uploadService.progressEmitter.subscribe(
      (progress) => {
        this.progress = progress;
      }
    );
    // Check fields
    if (this.uploadService.formIsValid(this.title, this.selectedFile)) {
      const dateTransformed = new Date(this.selectedFile!.lastModified);
      this.uploadService.saveDocumentService(
        this.title,
        this.year,
        this.selectedFile!,
        dateTransformed
      );
    } else {
      this.showErrorMessage = true;
      this.timeoutUsed = setTimeout(() => {
        this.showErrorMessage = false;
      }, 4500);
    }
  }

  // Delete confirm alert
  confirmationDelete(path: string, name: string): void {
    this.showPopupConfirmation = true;
    this.fileToDelete = name;
    this.fileDeleteUrl = path;
  }

  // File deletion
  deleteFile(path: string): void {
    this.showPopupConfirmation = false;
    this.uploadService.deleteFile(path);
    window.location.reload();
  }
}
