import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'EcoFriendly';
  showOutlet: boolean = false;

  constructor(private router: Router) {}

  // Show admin login template without showing header/footer
  modifyHeader() {
    // Using this instead of subscription prevents memory leaks because its from
    // the main AppComponent and we can't unsubscribe from it.
    // this executes when a new component is created
    this.showOutlet = this.router.url === '/admin' ? false : true;
  }
}
