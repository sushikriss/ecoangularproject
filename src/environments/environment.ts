// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore';

export const environment = {
  firebase: {
    apiKey: 'AIzaSyCehHrCUcFdk1cWtd1fnSsCX5em6vBPK8w',
    authDomain: 'ecofriendly-bf847.firebaseapp.com',
    databaseURL:
      'https://ecofriendly-bf847-default-rtdb.europe-west1.firebasedatabase.app',
    projectId: 'ecofriendly-bf847',
    storageBucket: 'ecofriendly-bf847.appspot.com',
    messagingSenderId: '403249774486',
    appId: '1:403249774486:web:aef608f5d7f122b3a1e7b7',
    measurementId: 'G-WELRH5B6VR',
  },
  production: false,
};

const app = initializeApp(environment.firebase);
const auth = getAuth(app);
const db = getFirestore();

export { auth, db };

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
