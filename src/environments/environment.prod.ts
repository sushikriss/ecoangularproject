import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore';

export const environment = {
  // Replace with your FireBase configuration

  firebase: {
    apiKey: 'your-key',
    authDomain: 'your-domain',
    databaseURL:
      'your-databaseURL',
    projectId: 'your-projectId',
    storageBucket: 'your-storageBucket',
    messagingSenderId: 'your-messagingSenderId',
    appId: 'id',
    measurementId: 'id',
  },
  production: true,
};

const app = initializeApp(environment.firebase);
const auth = getAuth(app);
const db = getFirestore();

export { auth, db };
